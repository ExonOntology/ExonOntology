# ======================================================================================================================
#
#	Package for the Exon Ontology analyses.
#
#	Package version v1.5.0 (last major update 2016/12)
#
# ======================================================================================================================

package ExonOntologyLib;

# ======================================================================================================================
#
#	Libraries
#
# ======================================================================================================================
use strict;
use base 'Exporter';
use Time::HiRes qw(gettimeofday tv_interval);
use DBI;

our @EXPORT = qw(openDBConnection closeDBConnection prepareSelectQuery executePreparedSelectQueryAndFetchAllArrayRef setStartTime getElapsedTime printElapsedTime locFSL plog alog);
our @EXPORT_OK = qw();

# ======================================================================================================================
#
#	Class fields
#
# ======================================================================================================================
my $DB_HOST = 'localhost';
my $DB_USER = 'exont';
my $DB_PWD = 'exont+pwd';
my $DB_NAME = 'ExonOntology_v1_5_0';
my $DBCONNECTION;
my $STARTTIME;

# ======================================================================================================================
#
#	A- DATABASE
#
# ======================================================================================================================
# ======================================================================================================================
#	Open and return a connection to the database (default database).
#		<-- $DBCONNECTION: the active connection to the default database.
# ======================================================================================================================
sub openDBConnection {
	if (!defined $DBCONNECTION) {
		if ((!defined $DB_HOST) || (!defined $DB_USER) || (!defined $DB_PWD) || (!defined $DB_NAME)) {
			plog("Problem with database connection: undef parameters (N:$DB_NAME, H:$DB_HOST, U:$DB_USER, P:*******).\n");
			return undef;
		} else {
			$DBCONNECTION = DBI -> connect('DBI:mysql:'.$DB_NAME.':'.$DB_HOST, $DB_USER, $DB_PWD) or die &locFSL . "Problem with database connection (N:$DB_NAME, H:$DB_HOST, U:$DB_USER, P:*******): $!\n";
			plog("Database connection created (N:$DB_NAME, H:$DB_HOST, U:$DB_USER, P:*******).\n");
			return $DBCONNECTION;
		}
	} else {
	 return $DBCONNECTION;
	}
}

# ======================================================================================================================
#	Close the current connection to the database.
# ======================================================================================================================
sub closeDBConnection {
	if (defined $DBCONNECTION) {
		plog("Current database connection closed.\n");
		$DBCONNECTION -> disconnect;
		undef $DBCONNECTION;
	} else {
		plog("No database connection to close.\n");
	}
}

# ======================================================================================================================
#	Prepare a query (for SELECT and the like).
#		--> $query: the select query to prepare.
#		--> $connection (optional): the connection to use for the preparation.
#		<-- $preparedStatment: the prepared statment.
# ======================================================================================================================
sub prepareSelectQuery {
	my ($query, $connection) = @_;
	if (!defined $connection) {
		$connection = openDBConnection;
	}
	my $preparedStatment = $connection -> prepare($query) or die &locFSL . "Error with the database on preparing a select query.\n\t" . $connection -> errstr;
	return $preparedStatment;
}

# ======================================================================================================================
#	Execute a prepared query (for SELECT and the like).
#		--> $preparedStatment: the prepaerd statment to execute.
#		--> $connection (optional): the connection to use for the execution.
#		--> $params_aref: an array ref that contains the parameters for the execution.
#		<-- $arrayref: the ArrayRef that contains the results of the query.
# ======================================================================================================================
sub executePreparedSelectQueryAndFetchAllArrayRef {
	my ($preparedStatment, $connection, $params_aref) = @_;
	if (!defined $connection) {
		$connection = openDBConnection;
	}
	if ((defined $params_aref) && (ref $params_aref eq 'ARRAY')) {
		$preparedStatment -> execute(@{$params_aref}) or die &locFSL . "Error with the database on executing a select query.\n\t" . $connection -> errstr; 
	} else {
		$preparedStatment -> execute() or die &locFSL . "Error with the database on executing a select query.\n\t" . $connection -> errstr; 
	}
	return $preparedStatment -> fetchall_arrayref;
}

# ======================================================================================================================
#
#	B- LOG
#
# ======================================================================================================================
# ======================================================================================================================
#	Set start time. It is useful to get the computing time in the end.
#		<-- $STARTTIME: the start time. 
# ======================================================================================================================
sub setStartTime {
	$STARTTIME = [gettimeofday];
	return $STARTTIME;
}

# ======================================================================================================================
#	Get elapsed time.
#		<-- $elapsed: the computing time in seconds. 
# ======================================================================================================================
sub getElapsedTime {
	my $elapsed = tv_interval($STARTTIME);
	return $elapsed;
}

# ======================================================================================================================
#	Print elapsed time.
# ======================================================================================================================
sub printElapsedTime {
	my $elapsed = getElapsedTime;
	my ($s, $m, $h) = gmtime($elapsed);
	plog('Script finished in ' . $h . 'h' . $m . 'm' . $s . "s.\n");
}

# ======================================================================================================================
#	Caller function for logging.
#		<-- $toRet: a tag that contains the time and the calling location.
# ======================================================================================================================
sub locFSL {
	my @all = caller(1);
	my $TimeOftheDay = localtime();
	my $toRet = '[' . $TimeOftheDay . ']';
	if (scalar(@all) >= 3) {
		$toRet .= '[' . $all[1] . '][' . $all[2] . '] ';
	} else {
		$toRet .= '[MAIN] ';
	}
	return $toRet;
}

# ======================================================================================================================
#	Log information (include header - caller).
#		--> $text: the text to print.
# ======================================================================================================================
sub plog {
	my ($text) = shift;
	return if (!defined $text);
	print STDERR locFSL . "$text";
}

# ======================================================================================================================
#	Log information (no header)
#		--> $text: the text to print.
# ======================================================================================================================
sub alog {
	my ($text) = shift;
	return if (!defined $text);
	print STDERR "$text";
}

1;
