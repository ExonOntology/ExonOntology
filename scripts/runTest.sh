echo 'Running Exon Ontology simple test...'
/usr/bin/perl scripts/EXONT_annotateSequences.pl -f ./testfiles/test81exons.tsv -b ASE -c 1 -s 2 -e 3 -m 5
echo ''
echo 'Checking the results...'
echo -ne '(1) Errors in matched sequences: '
diff ./testfiles/test81exons_matchedgenomiccoordinates.tsv ./testfiles/test81exons_matchedgenomiccoordinates.tsv.ref | wc -l
echo -ne '(2) Errors in matched exons: '
diff ./testfiles/test81exons_matchedexons.tsv ./testfiles/test81exons_matchedexons.tsv.ref | wc -l
echo -ne '(3) Errors in sequence file: '
diff ./testfiles/test81exons_matchedexons_sequences.tsv ./testfiles/test81exons_matchedexons_sequences.tsv.ref | wc -l
echo -ne '(4) Errors in annotation file: '
diff ./testfiles/test81exons_annotatedgenomiccoordinates.tsv ./testfiles/test81exons_annotatedgenomiccoordinates.tsv.ref | wc -l
echo -ne '(5) Errors in list of PTM annotation file: '
sort ./testfiles/test81exons_ptmcontextsequences.tsv > f1.tmp
sort ./testfiles/test81exons_ptmcontextsequences.tsv.ref > f2.tmp
diff f1.tmp f2.tmp | wc -l
rm f1.tmp f2.tmp
echo -ne '(6) Errors in list of feature file: '
sort ./testfiles/test81exons_features.tsv > f1.tmp
sort ./testfiles/test81exons_features.tsv.ref > f2.tmp
diff f1.tmp f2.tmp | wc -l
rm f1.tmp f2.tmp
