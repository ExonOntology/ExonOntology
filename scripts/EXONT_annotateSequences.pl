#!/usr/bin/perl 

# ============================================================================
#
#	This scripts takes sequences of interest defined by their chromosomal 
#	coordinates and annotate them with Exon Ontology terms.
#
#	Package version v1.5.0 (last major update 2016/12)
#
# ============================================================================

# ============================================================================
#	LIBRARIES
# ============================================================================
use lib "./lib/";
use strict;
use ExonOntologyLib;
use Getopt::Std;
use POSIX;
use List::Util qw(min max);
use Statistics::Distributions qw(uprob);
use Statistics::Multtest qw(BH);
use List::MoreUtils qw(zip);

# ============================================================================
#	SETUP AND PARAMETERS
# ============================================================================
setStartTime();
plog("[INFO] Script started.\n");
my $dataFolder = "./data/";

# Options:
#	-f file_name (mandatory): the file that contains the data to analyze.
#	-b background (mandatory): the background to compare with (for the 
#		statistics).
#	-c chromosome_index (optional): the index of the column that contains the 
#		chromosome (DEFAULT 1).
#	-s start_index (optional): the index of the column that contains the start
#		(DEFAULT 2).
#	-e end_index (optional): the index of the column that contains the end 
#		(DEFAULT 3).
#	-m min_nb_hits (optional): the minimum number of hits for the statistics 
#		(DEFAULT 5).
my %opts;
getopts 'f:b:c:s:e:m:', \%opts;
if (!exists $opts{'f'} || !exists $opts{'b'}) {
	plog("[ERROR] This script needs a file name (-f) and a background (-b).\n");
	plog('[INFO] Script ended in '.getElapsedTime()." seconds.\n");
	exit(0);
}
my $INDEX_CHROMOSOME = 1;
$INDEX_CHROMOSOME = $opts{'c'} if (exists $opts{'c'});
my $INDEX_START = 2;
$INDEX_START = $opts{'s'} if (exists $opts{'s'});
my $INDEX_END = 3;
$INDEX_END = $opts{'e'} if (exists $opts{'e'});
my $MIN_NB_HITS = 5;
$MIN_NB_HITS = $opts{'m'} if (exists $opts{'m'});

# SQL queries.
my $DBQuery = "SELECT DISTINCT `id_gene`, `official_symbol`, `pos_sur_gene`, e.`start_sur_chromosome`, e.`end_sur_chromosome`, `main_status`, `cds_max_length`, `cds_chrom_start`, `cds_chrom_end`, g.`start_sur_chromosome`, g.`end_sur_chromosome`, g.`strand`, e.`exon_types` FROM `hsapiens_exonsstatus_improved` e INNER JOIN `IMPORT_FasterDB_genes` g ON e.`id_gene` = g.`id` WHERE e.`chromosome`= ? AND ((e.`start_sur_chromosome` >= ? AND e.`start_sur_chromosome` <= ?) OR (e.`end_sur_chromosome` >= ? AND e.`end_sur_chromosome` <= ?) OR (e.`start_sur_chromosome` <= ? AND e.`end_sur_chromosome` >= ?))";
my $DBQuery_gSeq = "SELECT sequence FROM IMPORT_FasterDB_genes WHERE `id`= ? ";
my $DBQuery_pSeq = "SELECT `peptide_sequence`, `fragment_start_on_gene`, `fragment_end_on_gene`, `offset_before_exon`, COUNT(DISTINCT `transcript_id`) FROM `hsapiens_exonpeptides` WHERE `gene_id`= ? AND `fragment_start_on_gene` <= ? AND `fragment_end_on_gene` >= ? AND `peptide_sequence` != '' GROUP BY `peptide_sequence`, `fragment_start_on_gene`, `fragment_end_on_gene`, `offset_before_exon` ORDER BY 5 DESC LIMIT 1";
my $DBQuery_select = "SELECT DISTINCT d.`eo_id`, t.`eo_fullid`, t.`eo_name`, SUM(DISTINCT d.`annotation_code`), t.`categories`, t.`paths`, ";
$DBQuery_select .= "d.`original_eo_id`, d.`chromosome_start`, d.`chromosome_end`, MAX(d.`original_hitsize`) ";
$DBQuery_select .= "FROM `EO_hsapiens_annotations` d INNER JOIN `EO_hsapiens_ontology`t ON d.`eo_id` = t.`eo_id` WHERE ";
my $DBQuery_where = "`gene_id` = ? AND `genomic_exon` = ? AND `chromosome` = ? AND ";
$DBQuery_where .= "((`chromosome_start` <= ? AND `chromosome_end` >= ?) OR (`chromosome_start` <= ? AND `chromosome_end` >= ?) OR (`chromosome_start` >= ? AND `chromosome_end` <= ?)) ";
$DBQuery_where .= "GROUP BY d.`eo_id`, d.`original_eo_id`, d.`chromosome_start`, d.`chromosome_end`";
my $DBQuery_selectSeq = "SELECT DISTINCT d.`eo_id`, t.`eo_fullid`, t.`eo_name`, SUM(DISTINCT d.`annotation_code`), t.`categories`, ";
$DBQuery_selectSeq .= "`original_featuresequence_before`, `original_featuresequence`, `original_featuresequence_after` ";
$DBQuery_selectSeq .= "FROM `EO_hsapiens_allptm_sequences` d INNER JOIN `EO_hsapiens_ontology`t ON d.`eo_id` = t.`eo_id` WHERE (`annotation_code` = 16 OR `annotation_code` = 48) AND ";
my $DBQuery_whereSeq = "`gene_id` = ? AND `genomic_exon` = ? AND `chromosome` = ? AND ";
$DBQuery_whereSeq .= "((`chromosome_start` <= ? AND `chromosome_end` >= ?) OR (`chromosome_start` <= ? AND `chromosome_end` >= ?) OR (`chromosome_start` >= ? AND `chromosome_end` <= ?)) ";
$DBQuery_whereSeq .= "GROUP BY d.`eo_id`, d.`original_eo_id`, d.`chromosome_start`, d.`chromosome_end`, `original_featuresequence_before`, `original_featuresequence`, `original_featuresequence_after`";

# ============================================================================
#	IDENTIFY THE FILE TAG
# ============================================================================
my @fileNameElements = split '\.', $opts{'f'};
pop @fileNameElements if ((scalar @fileNameElements  > 1) 
	&& (length $fileNameElements[-1] <= 4));
my $fileTag = join '.', @fileNameElements;
my $shortFileTag = (split '\/', $fileTag) [-1];
plog("[INFO] [$shortFileTag] Analysis started.\n");

# ============================================================================
#	READ LIST OF GENOMIC POSITIONS
# ============================================================================
open FIN, '<'.$opts{'f'} 
	or die "File with genomic positions does not exist.\n";
my $HEADERS = <FIN>;
chomp $HEADERS;
my @POSITIONS = <FIN>; # ...,chr, start, end, ...
close FIN;
plog("[INFO] [$shortFileTag] " . scalar(@POSITIONS) . " genomic positions found.\n");

# ============================================================================
#	PREPARE SQL
# ============================================================================
my @DBRows = ();
my @DBRows_gSeq = ();
my @DBRows_pSeq = ();
my $DBConn = openDBConnection();
my $DBStmt = prepareSelectQuery($DBQuery, $DBConn);
my $DBStmt_gSeq = prepareSelectQuery($DBQuery_gSeq, $DBConn);
my $DBStmt_pSeq = prepareSelectQuery($DBQuery_pSeq, $DBConn);
my $DBQueryAnnot = $DBQuery_select . $DBQuery_where;
my $DBStmtAnnot = prepareSelectQuery($DBQueryAnnot, $DBConn);
my $DBQuerySeq = $DBQuery_selectSeq . $DBQuery_whereSeq;
my $DBStmtSeq = prepareSelectQuery($DBQuerySeq, $DBConn);
plog("[INFO] [$shortFileTag] Database ready.\n");

# ============================================================================
#	MAP EACH GENOMIC POSITION TO ONE OR SEVERAL FDB EXON
# ============================================================================
open FOUT, '>' . $fileTag . '_matchedexons.tsv';
open FOUTS, '>' . $fileTag . '_matchedexons_sequences.tsv';
open FOUTC, '>' . $fileTag . '_matchedgenomiccoordinates.tsv';
print FOUT "$HEADERS\tGlobal tag\tNb FasterDB exons\tFasterDB exons\n";
print FOUTS "$HEADERS\tSequences\n";
print FOUTC "Sequence name\tFasterDB gene\tFasterDB exon\t";
print FOUTC "Sequence chromosome\tSequence start\tSequence end\n";
my $nbLines = 0;
my $k = 0;
# For each sequence in the file, represented as coordinates.
foreach (@POSITIONS) {

	# Chomp the line (win and nux).
	$/ = "\r\n";
	chomp;
	$/ = "\n";
	chomp;
	my @elements = split '\t', $_;

	# Skip empty lines.
	if (scalar(@elements) < 4) {
		print FOUT $_ . "\t\t\t\n";
		print FOUTS $_ . "\t\n";
		$nbLines++;
		next;
	}
	
	# Prepare SQL parameters.
	my @params = ();
	push @params, $elements[$INDEX_CHROMOSOME];
	push @params, $elements[$INDEX_START];
	push @params, $elements[$INDEX_END];
	push @params, $elements[$INDEX_START];
	push @params, $elements[$INDEX_END];
	push @params, $elements[$INDEX_START];
	push @params, $elements[$INDEX_END];
	@DBRows = @{executePreparedSelectQueryAndFetchAllArrayRef($DBStmt, $DBConn, \@params)};

	# For each database row, i.e., each exon represented by 
	# [0-2] `id_gene`, `official_symbol`, `pos_sur_gene`, 
	# [3-5] `start_sur_chromosome`, `end_sur_chromosome`, `main_status`, 
	# [6-8] `cds_max_length`, `cds_chrom_start`, `cds_chrom_end`, 
	# [9-10] gene.`start_sur_chromosome`, gene.`end_sur_chromosome`, 
	# [11-12] gene.`strand`, exon.`exon_types`
	my @matchedExons = ();
	my @matchedExonSequences = ();
	foreach my $row_aRef (@DBRows) {
		my @aRow = @{$row_aRef};

		# Put into categories with tags that qualify the match.
		# N stands for 'Undefined', E for 'Exact', T for 'Total inclusion', 
		# and P for 'Partial inclusion'.
		my $tag = 'UU';
		if (($aRow[3] == $elements[$INDEX_START]) && ($aRow[4] == $elements[$INDEX_END])) {
			$tag = 'EE';
		} elsif($aRow[3] == $elements[$INDEX_START]) { # Start matches (but not end).
			if ($aRow[4] > $elements[$INDEX_END]) {
				$tag = 'EP';
			} else { # Therefore, we have: $aRow[4] < $elements[$INDEX_END].
				$tag = 'ET';
			}
		} elsif($aRow[4] == $elements[$INDEX_END]) { # End matches (but not start).
			if ($aRow[3] < $elements[$INDEX_START]) {
				$tag = 'PE';
			} else { # Therefore, we have: $aRow[3] > $elements[$INDEX_START].
				$tag = 'TE';
			}
		} else { # No macth whatsoever.
			if ($aRow[3] > $elements[$INDEX_START]) {
				if ($aRow[4] < $elements[$INDEX_END]) {
					$tag = 'TT';
				} else { # Therefore, we have: $aRow[4] > $elements[$INDEX_END].
					$tag = 'TP';
				}
			} else { # Therefore, we have: $aRow[3] < $elements[$INDEX_START].
				if ($aRow[4] < $elements[$INDEX_END]) {
					$tag = 'PT';
				} else { # Therefore, we have: $aRow[4] > $elements[$INDEX_END].
					$tag = 'PP';
				}
			}
		} # End if exact match or ...

		# Quantify the match / overlap.
		my $overlapStart = $elements[$INDEX_START];
		$overlapStart = $aRow[3] if $aRow[3] > $elements[$INDEX_START];
		my $overlapEnd = $elements[$INDEX_END];
		$overlapEnd = $aRow[4] if $aRow[4] < $elements[$INDEX_END];
		my $overlapLength = $overlapEnd - $overlapStart + 1;
		my $ratioOnOriginalInput = 'NA';
		$ratioOnOriginalInput = int(10000 * $overlapLength / ($elements[$INDEX_END] - $elements[$INDEX_START] + 1)) / 100 
			if (($elements[$INDEX_END] - $elements[$INDEX_START] + 1) > 0);
		my $ratioOnFDBExon = 'NA';
		$ratioOnFDBExon = int(10000 * $overlapLength / ($aRow[4] - $aRow[3] + 1)) / 100
			if (($aRow[4] - $aRow[3] + 1) > 0);

		# We then identify the coding sequences of the overlap (overlap
		# between the user provided regions and the FasterDB exons).
		my $overlapCDSStart = '';
		my $overlapCDSEnd = '';
		my $overlapCDSLength = '0';
		
		# If either the start or the end of the coding sequence is within the 
		# overlap, or if the coding sequence fully include the overlap.
		if ((($aRow[7] >= $overlapStart) && ($aRow[7] <= $overlapEnd)) || 
			(($aRow[8] >= $overlapStart) && ($aRow[8] <= $overlapEnd)) || 
			(($aRow[7] < $overlapStart) && ($aRow[8] > $overlapEnd))) {

			$overlapCDSStart = $overlapStart;
			$overlapCDSStart = $aRow[7] if $aRow[7] > $overlapStart;
			$overlapCDSEnd = $overlapEnd;			
			$overlapCDSEnd = $aRow[8] if $aRow[8] < $overlapEnd;
			$overlapCDSLength = $overlapCDSEnd - $overlapCDSStart + 1;
		}

		# Only save the exon if it is not associated to NA.
		# The first 6 fields are for the FasterDB exon data, then the next 6 
		# are for the actual overlap with the region, and the last 3 are for 
		# the overlap coding sequence.
		if ($ratioOnFDBExon ne 'NA' && $ratioOnOriginalInput ne 'NA') {
			$aRow[12] =~ s/ACE/ASE/;
			$aRow[12] =~ s/CCE/CE/;
			my $localElement = "$aRow[0];$aRow[1];$aRow[2];$aRow[3];$aRow[4];";
			$localElement .= "$aRow[5];$aRow[6];$tag;$overlapStart;$overlapEnd;";
			$localElement .= "$overlapLength;$ratioOnOriginalInput;";
			$localElement .= "$ratioOnFDBExon;$overlapCDSStart;$overlapCDSEnd;";
			$localElement .= "$overlapCDSLength;$aRow[12]";
			push @matchedExons, $localElement;
			
			# Get the genomic sequence of the exon.
			# Prepare SQL parameters ('id_gene' in this case).
			my @params_gSeq = ();
			push @params_gSeq, $aRow[0];
			@DBRows_gSeq = @{executePreparedSelectQueryAndFetchAllArrayRef(
				$DBStmt_gSeq, $DBConn, \@params_gSeq)};

			# For each database row, i.e., each sequence (`sequence`).
			my $genomicSequence = '';
			my $CDSgenomicSequence = '';
			my $geneStartOnChr = $aRow[9];
			my $geneEndOnChr = $aRow[10];
			my $geneSense = $aRow[11];
			my $overlapCDSStartOnGene = 0;
			my $overlapCDSEndOnGene = 0;
			# There should be only one gene for a given id.
			foreach my $row_gSeq_aRef (@DBRows_gSeq) {
				my @aRow_gSeq = @{$row_gSeq_aRef};
				
				# If it is a sense gene.
				if ($geneSense eq '1') {
					$genomicSequence = substr $aRow_gSeq[0], $overlapStart - $geneStartOnChr, $overlapLength;
					$overlapCDSStartOnGene = $overlapCDSStart - $geneStartOnChr if $overlapCDSLength > 0;
				} else { # The gene is anti-sense (strand is -1).
					$genomicSequence = substr $aRow_gSeq[0], $geneEndOnChr - $overlapEnd, $overlapLength;
					$overlapCDSStartOnGene = $geneEndOnChr - $overlapCDSEnd if $overlapCDSLength > 0;
				}
				
				# If we have a CDS, get the sequence.
				if ($overlapCDSLength > 0) {
					$overlapCDSEndOnGene = $overlapCDSStartOnGene + $overlapCDSLength;
					$CDSgenomicSequence = substr $aRow_gSeq[0], $overlapCDSStartOnGene, $overlapCDSLength;
				}
			}
			
			# Get the associated peptide if the exon contains a CDS.
			my %peptides = ();
			if ($overlapCDSLength > 0) {

				# Prepare SQL parameters ('id_gene', oCDSstart + oCDSend 
				# in this case).
				my @params_pSeq = ();
				push @params_pSeq, $aRow[0];
				
				# We add +1 since previously, it was the substr offset not 
				# the real start, now it is.
				push @params_pSeq, $overlapCDSStartOnGene + 1;
				push @params_pSeq, $overlapCDSEndOnGene;
				@DBRows_pSeq = @{executePreparedSelectQueryAndFetchAllArrayRef($DBStmt_pSeq, $DBConn, \@params_pSeq)};

				# For each database row, i.e., each sequence fragment
				# `peptide_sequence`, `fragment_start_on_gene`, 
				# `fragment_end_on_gene`, `offset_before_exon`
				# There should be only one in fact.
				foreach my $row_pSeq_aRef (@DBRows_pSeq) {

					# Get the peptide fragment that corresponds to the CDS.
					my @aRow_pSeq = @{$row_pSeq_aRef};
					my $overlapCDSStartOnFrag = floor(($overlapCDSStartOnGene + 1 - ($aRow_pSeq[1] - $aRow_pSeq[3])) / 3);
					my $overlapCDSEndOnFrag = ceil(($overlapCDSEndOnGene - ($aRow_pSeq[1] - $aRow_pSeq[3]) + 1) / 3);
					# No +1 since the start is in fact the substring offset 
					# not the real start.
					my $overlapCDSLengthOnFrag = $overlapCDSEndOnFrag - $overlapCDSStartOnFrag;
					my $peptideSequence = substr $aRow_pSeq[0], $overlapCDSStartOnFrag, $overlapCDSLengthOnFrag;

					# Keep the data to print.
					$peptides{$peptideSequence} = 1;
				}
			}

			# Keep the sequences to print.
			my $localAllSequences = $genomicSequence . ';' . $CDSgenomicSequence . ';' . join('|', keys %peptides);
			push @matchedExonSequences, $localAllSequences;
		} else {
			push @matchedExons, '';	
			push @matchedExonSequences, '';	
		}
	} # End foreach database row.

	# Keep exon data.
	my $globalTag = 'None';
	if (scalar @matchedExons == 1) {
		$globalTag = 'Single';
	} elsif (scalar @matchedExons > 1) {
		$globalTag = 'Multiple';
	}
			
	# Print the data.
	print FOUT $_ . "\t$globalTag\t" . scalar(@matchedExons) . "\t";
	print FOUT join(',', @matchedExons) . "\n";
	print FOUTS $_ . "\t" . join(',', @matchedExonSequences) . "\n";

	# Print the macthed data for next step
	foreach my $mm (@matchedExons) {
		my @mmEls = split ';', $mm;
		print FOUTC $elements[0] . "\t" . $mmEls[0] . "\t" . $mmEls[2] . "\t";
		print FOUTC $elements[$INDEX_CHROMOSOME] . "\t" . $mmEls[13] . "\t";
		print FOUTC $mmEls[14] . "\n";
		$k += ($mmEls[14] - $mmEls[13] + 1);
	}

	$nbLines++;
}
close FOUT;
close FOUTS;
close FOUTC;

plog("[INFO] [$shortFileTag] Mapping done ($nbLines printed).\n");

# Second round parameters
my $INDEX_GENE = 1;
my $INDEX_EXON = 2;
$INDEX_CHROMOSOME = 3;
$INDEX_START = 4;
$INDEX_END = 5;

# ============================================================================
#	READ LIST OF MATCHED POSITIONS
# ============================================================================
open FIN, '<' . $fileTag . '_matchedgenomiccoordinates.tsv' 
	or die "Position list does not exist.\n";
my $TITLE = <FIN>;
chomp $TITLE;
# ... \t gene \t exon \t chromosome \t strand \t start \t end \t ...
@POSITIONS = <FIN>;
close FIN;
plog("[INFO] [$shortFileTag] " . scalar(@POSITIONS) . " genomic positions found.\n");

# ============================================================================
#	READ BACKGROUND DATA AND FIND CLOSEST K
# ============================================================================
my $closestK = '-25';
my %mus = ();
my %sigmas = ();
open FIN, '<' . $dataFolder . $opts{'b'} . '_background.tsv' 
	or die "Background data does not exist.\n";
my $smallerK = -1;
my $largerK = 999999999;
my %ks = ();
foreach (<FIN>) {  # sampleSize  featureId  mu  sigma
	chomp;
	my @tmp = split "\t", $_;
	$ks{$tmp[0]} = 1;
	$mus{$tmp[0] . '|' . $tmp[1]} = $tmp[2];
	$sigmas{$tmp[0] . '|' . $tmp[1]} = $tmp[3];

	# Find closest k.
	if (($tmp[0] >= $k) && ($tmp[0] < $largerK)) {
		$largerK = $tmp[0];
	} elsif (($tmp[0] <= $k) && ($tmp[0] > $smallerK)) {
		$smallerK = $tmp[0];
	}
}
close FIN;
plog("[INFO] [$shortFileTag] background read (mus and sigmas).\n");
$closestK = $smallerK;
$closestK = $largerK if (($largerK - $k) < ($k - $smallerK));
plog("[INFO] [$shortFileTag] closest control sample size identified ($k ~~ $closestK).\n");

# ============================================================================
#	ORGANIZE THE ONTOLOGICAL ASSOCIATIONS FOR EACH POSITION
# ============================================================================
open FOUT1, '>' . $fileTag . '_annotatedgenomiccoordinates.tsv';
print FOUT1 "$TITLE\tEXONT id\tEXONT fullid\tEXONT name\tCategories\n";
open FOUT3, '>' . $fileTag . '_ptmcontextsequences.tsv';
print FOUT3 "$TITLE\tEXONT id\tEXONT fullid\tEXONT name\tCategories\tSequence before feature\tFeature sequence\tSequence after feature\n";
my %featureData = ();
my %featureData_scores = ();
my %featureChildren = ();

# For each sequence to annotate, represented as coordinates.
foreach (@POSITIONS) {

	# Chomp lines again.
	$/ = "\r\n"; chomp;
	$/ = "\n"; chomp;

	# Prepare SQL parameters.
	my @params = ();
	my @tmp = split '\t', $_;
	my $gene = $tmp[$INDEX_GENE];
	my $exon = $tmp[$INDEX_EXON];
	my $chromosome = $tmp[$INDEX_CHROMOSOME];
	my $start = $tmp[$INDEX_START];
	my $end = $tmp[$INDEX_END];
	push @params, $gene;
	push @params, $exon;
	push @params, $chromosome;
	push @params, $start;
	push @params, $start;
	push @params, $end;
	push @params, $end;
	push @params, $start;
	push @params, $end;
	my @DBRows = @{executePreparedSelectQueryAndFetchAllArrayRef($DBStmtAnnot, $DBConn, \@params)};

	# We collect the PTM context sequences.
	my @DBSeqs = @{executePreparedSelectQueryAndFetchAllArrayRef($DBStmtSeq, $DBConn, \@params)};
	my %PTMSequences = ();
	foreach my $row_aRef (@DBSeqs) {
		my @aRow = @{$row_aRef};
		$PTMSequences{"$_\t$aRow[0]\t$aRow[1]\t$aRow[2]\t$aRow[4]\t$aRow[5]\t$aRow[6]\t$aRow[7]"} = $aRow[4] . '|' . $aRow[1];
	}

	# Print annotations sorted by feature category and identifier.
	print FOUT3 join("\n", sort {$PTMSequences{$a} cmp $PTMSequences{$b}}(keys %PTMSequences)) . "\n"
		if (scalar(keys %PTMSequences) > 0);
	
	# For each database row, i.e., each annotation, represented as  
	# `eo_id` `eo_fullid` `eo_name` `annotation_code` `categories` `paths`
	# `original_eo_id` `chromosome_start` `chromosome_end` `original_hitsize`
	# Hash to sort the features by category and eo_fullid.
	my %sortedRows = ();
	foreach my $row_aRef (@DBRows) {
		my @aRow = @{$row_aRef};

		# Compute the score for this annotation.
		my $localScore = (min($aRow[8], $end) - max($aRow[7], $start) + 1)  / $aRow[9];

		# Print the features that are directly annotated (sum is 16 or 48), 
		# split by category (for features with multiple categories)
		if (($aRow[3] == 16) || ($aRow[3] == 48)) {
			foreach my $cat (sort split ',', $aRow[4]) {
				$sortedRows{"$_\t$aRow[0]\t$aRow[1]\t$aRow[2]\t$cat"} = $cat . '|' . $aRow[1];
			}
		}

		# Organize features for the next print (all features)
		my $featureDataKey = "$aRow[0]\t$aRow[1]\t$aRow[2]\t$aRow[4]";
		my %annotatedPositions = ();
		if (exists($featureData{$featureDataKey})) {
			%annotatedPositions = %{$featureData{$featureDataKey}};
		}
		$annotatedPositions{join(';', sort split('\t', $_))} = 1;
		$featureData{$featureDataKey} = \%annotatedPositions;

		# Update the score for this ontological term.
		if (exists($featureData_scores{$featureDataKey})) {
			$featureData_scores{$featureDataKey} += $localScore;
		} else {
			$featureData_scores{$featureDataKey} = $localScore;
		}
		
		# Analyse the paths to build the children map.
		foreach my $path (split ',', $aRow[5]) {
			my @pathElements = split '-->', $path;

			# Update EO children map.
			my %localChildren = ();
			%localChildren = %{$featureChildren{$pathElements[1]}}
				if (exists($featureChildren{$pathElements[1]}));
			$localChildren{$aRow[1]} = 1;
			$featureChildren{$pathElements[1]} = \%localChildren;
		}
		
	} # End foreach database row
	
	# Print annotations sorted by feature category and identifier.
	print FOUT1 join("\n", sort {$sortedRows{$a} cmp $sortedRows{$b}}(keys %sortedRows)) . "\n"
		if (scalar(keys %sortedRows) > 0);
}
close FOUT1;
close FOUT3;
closeDBConnection();
plog("[INFO] [$shortFileTag] Position annotations file created.\n");

# ============================================================================
#	PRINT DETAILS FOR THE FEATURES
# ============================================================================
my @toPrint = ();
my @pvalues = ();
foreach my $eoKey (keys %featureData) {
	my %annotatedPositions = %{$featureData{$eoKey}};
	my $nbPositions = scalar(keys %annotatedPositions);
	my $totalScore = $featureData_scores{$eoKey};
	
	my @tmp = split "\t", $eoKey;
	my $eoId = $tmp[0];
	my $eoFullId = $tmp[1];
	my $children = '';
	$children = join ',', sort keys %{$featureChildren{$eoFullId}}
		if (exists $featureChildren{$eoFullId});

	# Compute stats
	my $logScore = log(($totalScore / (0.001 * $k)) + 1);
	my $zscore = '';
	my $pval = '';

	# We only compute the statistics for the testable features.
	if (exists $mus{$closestK . '|' . $eoId}) {
		$zscore = 'NA';
		$pval = 'NA';

		# We only compute the statistics when we have at least 
		# $MIN_NB_HITS hits.
		if ($nbPositions >= $MIN_NB_HITS) {
			my $mu = $mus{$closestK . '|' . $eoId};
			my $sigma = $sigmas{$closestK . '|' . $eoId};
			
			# Because we only compute an enrichement score (and not a
			# depletion score).
			$zscore = ($logScore - $mu) / $sigma;
			$pval = 1;
			if ($logScore > $mu) {
				$pval = uprob($zscore);
				push @pvalues, $pval;
			}
		}
	}

	# We compute the corrected score, that is the raw score divided by the 
	# sample size (in kilobases).
	my $correctedTotalScore = $totalScore / (0.001 * $k);
	push @toPrint, "$eoKey\t$nbPositions\t" . join(',', sort keys %annotatedPositions) . "\t$children\t$correctedTotalScore\t$zscore\t$pval";
}

# Compute the FDR corrected p-value.
my %FDR_BH = ();
%FDR_BH = zip @pvalues, @{BH(\@pvalues)} if (scalar @pvalues > 0);
open FOUT2, '>' . $fileTag . '_features.tsv';
print FOUT2 "EXONT id\tEXONT full id\tEXONT name\tCategories\t";
print FOUT2 "User sequence count\tUser sequences\tChildren\tScore\tZ-score\t";
print FOUT2 "P-value\tFDR\n";
foreach (@toPrint) {
	my @tmp = split "\t", $_;
	my $fdr = '';
	if (defined $tmp[9]) {
		$fdr = $tmp[9];
		if (exists $FDR_BH{$tmp[9]}) {
			$fdr = $FDR_BH{$tmp[9]};
		}
	}
	print FOUT2 $_ . "\t$fdr\n";
}
close FOUT2;
plog("[INFO] [$shortFileTag] gpFeature annotations file created.\n");

plog("[INFO] [$shortFileTag] Script ended in " . getElapsedTime() . " seconds.\n");

1;
