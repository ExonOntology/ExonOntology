CREATE DATABASE ExonOntology_v1_5_0; 
CREATE USER 'exont'@'localhost' IDENTIFIED BY 'exont+pwd';
GRANT ALL ON ExonOntology_v1_5_0.* TO 'exont'@'localhost';
