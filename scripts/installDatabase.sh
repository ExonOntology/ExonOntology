echo -ne 'Extracting the data...'
tar -xzf ./data/ExonOntology_v1.5.0.tar.gz -C ./data/
echo 'done.'
echo -ne 'Uploading the data to the database...'
mysql --defaults-extra-file='./scripts/.mylogin.cnf' ExonOntology_v1_5_0  < ./data/ExonOntology_v1.5.0.sql
echo 'done.'
echo -ne 'Cleaning the directory...'
rm ./data/ExonOntology_v1.5.0.sql
echo 'done.'
