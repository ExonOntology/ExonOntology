EXONT: Exon Ontology

# 1. Introduction
This project consists of a set of scripts that are necessary to perform offline Exon Ontology analyses. Briefly, it can accept a list of genomic sequences as input that represent exons (or parts of exons). The algorithm then retrieves the protein features that are encoded by these DNA sequences.

For more details about the algorithm, readers are referred to our manuscript and the [Exon Ontology website](http://fasterdb.ens-lyon.fr/ExonOntology/).

# 2. Installation
## 2.1. Dependencies
Exon Ontology scripts are provided as Perl scripts that rely on a MySQL database, so the user need to install mysql and perl before installing Exon Ontology.

In addition, the following Perl packages are required for the statistical analyses:
- Statistics::Distributions
- Statistics::Multtest

Last, Exon Ontology also needs the following packages that are often distributed along with Perl:
- DBI
- List::MoreUtils

## 2.2. Exon Ontology installation
It is first important to configure MySQL by creating the Exon Ontology specific user and database.
```bash
$> mysql -u root -p < ./scripts/configureDatabase.sql
```

Once the database is configured, it is necessary to upload the Exon Ontology data into the newly created database. This can take several minutes since the database is relatively large.
```bash
$> bash ./scripts/installDatabase.sh
```
# 2.3. Simple test
The Exon Ontology comes with a simple test file that can be used to check that the installation ended successfully.
```bash
$> bash ./scripts/runTest.sh
```
If the Exon Ontology has been successfully installed, then there should be no errors in the output:

```
Checking the results... 
(1) Errors in matched sequences: 0 
(2) Errors in matched exons: 0 
(3) Errors in sequence file: 0 
(4) Errors in annotation file: 0 
(5) Errors in list of PTM annotation file: 0 
(6) Errors in list of feature file: 0 
```

If there are errors, it means that something went wrong. Please refer to the troubleshooting section for more help on this matter.

# 3. Usage
Exon Ontology can be used from the command line by calling the *EXONT_annotateSequences.pl* script. This script takes a text file that contains sequences defined by genomic coordinates and will produce files that contain the Exon Ontology annotations.

## 3.1. Input
The input is a text file that contains sequences defined by genomic coordinates (*i.e.*, chromosome, start and end). It is also necessary to add a first column that contains the name of the sequences to analyze for an easier parsing of the results. For instance, the following lines represent a well-formatted set of sequences:
```
ADD3_16_down	10	111892063	111892158
CA12_9_down	15	63620297	63620329
DLG1_24_down	3	196802708	196802741
ENAH_12_down	1	225692693	225692755
```

Notice that the user has to indicate to the program which columns contain which data, should the order of the columns be different of the one above (sequence_name, chromosome, start, end).

The program assumes that coordinates are provided for human genome build hg19.

## 3.2. Script parameters
The Exon Ontology script requires two parameters and can accept four options.

The first required parameter is the path to the file that contains the sequences to analyze, and is controlled by the option *-f*. Please bear in mind that the additional files will be created in the same directory and will be named similarly.

The second required option is the background that should be used for the statistical analysis, and is controlled by the option *-b*. The user has the choice between five backgrounds just like from the web interface:
- AFE: alternative first coding exons; 
- ALE: alternative last coding exons;
- ASE: alternative coding exons (excluding terminal exons; AFE and ALE);
- CE: constitutive coding exons;
- ALL: all coding exons (*i.e.*, all of the above).

Therefore, a minimal command line can be of the following form.
```bash
$> /usr/bin/perl scripts/EXONT_annotateSequences.pl -f ./testfiles/test81exons.tsv -b ASE
```
This in fact replicates the simple test from section 2.3 by analyzing a list of 81 exons and comparing them with other alternative exons (ASE).

The script also accepts the following options:
- *-c*, to define the column index that contains the chromosome, default is 1.
- *-s*, to define the column index that contains the start of the sequence, default is 2.
- *-e*, to define the column index that contains the end of the sequence, default is 3.
- *-m*, to define the minimum number of sequences required to compute an enrichment, default is 5.

Notice that the indices are 0-based and that for the time being, the sequence name column is always expected to be the first column (*i.e.*, on position 0).

Therefore, a more complex but equivalent to the one above is the following:
```bash
$> /usr/bin/perl scripts/EXONT_annotateSequences.pl -f ./testfiles/test81exons.tsv -b ASE -c 1 -s 2 -e 3 -m 5
```
## 3.3. Outputs
The algorithm produces six files with the results. These files correspond to the tables that can be obtained from the Exon Ontology web server. The file names are derived from the input file name, with the following extensions:
- *matchedexons*: the FasterDB exons that are matching the user provided sequences.
- *matchedgenomiccoordinates*: more precise matching results with the coding sequences positions.
- *matchedexons_sequences*: the DNA sequences, DNA coding sequences, and protein sequences of the user defined sequences.
- *annotatedgenomiccoordinates*: the Exon Ontology annotations for all user defined sequences. Each sequence is annotated separately.
- *ptmcontextsequences*: The Exon Ontology PTM annotations for all user defined sequences. This represents a subset of the above file with only the PTM annotations, and with extra columns that contains the protein sequences around the PTM sites.
- *features*: the Exon Ontology features of the whole sequence set. Hits are summarized by feature. Frequent features are associated with a statistical test to assess the significance of the enrichment.

# 4. Troubleshooting
#### Why is there no strand in the input file?
There is no strand in the current version because we have made sure that there is no overlap between all human coding sequences. So, any set of coordinates can only match to either the positive or the negative strand, but never to both. This might not stand true in future releases.

#### Which human build is used?
Exon Ontology relies on the hg19 genome build.

#### Why is there a difference between the online and the command line versions?
Unfortunately, the dbPTM database did not agree on releasing their data. Therefore, the dbPTM data can only be accessed through the web server, and is not present in the database associated with the command line version.

#### Is Exon Ontology available for species *XXXX*?
So far, Exon Ontology is only available for human. We have yet no plan to extend it to other organisms.

#### What is the difference between the *matchedexons* and the *matchedgenomiccoordinates* output?
A user can input a sequence that corresponds to a non coding exon. This exon will then be present in the *matchedexons* file to indicate that the user sequence has been recognized. However, it won't be in the *matchedgenomiccoordinates* file since it is non coding and has therefore no protein feature.

# 5. Version and license
This is version 1.5.0 of Exon Ontology, which corresponds to the version used in our manuscript (see below).

This software and the associated data are licensed under the CC-BY-NC-SA license, and is thus free for academic use. Please contact us to define an agreement for a commercial use of our software.

If you have used Exon Ontology in your research, we would appreciate if you could cite our paper
> Tranchevent L.C., Aubé F., Dulaurier L., Benoit-Pilven C., Rey A., Poret A.,
> Chautard E., Mortada H., Desmet F.O., Zahra Chakrama F., Moreno-Garcia M.A.,
> Goillot E., Janczarski S., Mortreux F., Bourgeois C.F., Auboeuf D., 
> **Identification of protein features encoded by alternative exons using Exon Ontology**
> *Genome Res. (2017)*, doi: 10.1101/gr.212696.116

# 6. Online resources
Please refer to the [Exon Ontology website](http://fasterdb.ens-lyon.fr/ExonOntology/) or our manuscript (see above) for more details about our approach.
